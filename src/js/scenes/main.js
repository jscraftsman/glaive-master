const BASE_ASSET_PATH = '../src/assets/images';

export default class MainScene extends Phaser.Scene {
    constructor() {
        super({
            key: 'mainScene'
        });
    }

    preload() {
		this.load.animation('ybotData', `${BASE_ASSET_PATH}/ybot-animations.json`);
		this.load.atlas('ybot', `${BASE_ASSET_PATH}/ybot.png`, `${BASE_ASSET_PATH}/ybot.json`);
		
	}

    create() {
		this.scale.lockOrientation(Phaser.Scale.Orientation.LANDSCAPE);

		const fullScreenButton = this.add.text(10, 10, 'Toggle Fullscreen', { fill: '#0f0', fontSize: '32px'})
			.setInteractive()
			.on('pointerdown', () =>{
				if (this.scale.isFullscreen) {
					this.scale.stopFullscreen();
				} else {
					this.scale.startFullscreen();
				}	
			});

		this.plugins.get('rexAnchor').add(fullScreenButton, {
            x: `left+10`,
            y: `top+5`
        });
		
		const joystickRadius = 100;
		let joyStickBase = this.add.graphics().fillStyle(0x666666).fillCircle(0, 0, joystickRadius);
		let joyStickThumb = this.add.graphics().fillStyle(0xcccccc).fillCircle(0, 0, joystickRadius / 2);
		
        var joyStick = this.plugins.get('rexVirtualJoyStick').add(this, {
            x: 0,
            y: 0,
			base: joyStickBase,
            thumb: joyStickThumb,
            radius: joystickRadius
        });
		
		// TEMP - ANCHOR
		// joyStick.setOrigin(0, 1);
		this.plugins.get('rexAnchor').add(joyStick, {
            x: `left+${joystickRadius * 1.3}`,
            y: `bottom-${joystickRadius * 1.5}`
        });
		
		// HACK to set initial position of joystick thumb
		joyStickThumb.x = joyStickBase.x;
		joyStickThumb.y = joyStickBase.y;

		let obj = this.add.sprite(this.scale.width / 2, this.scale.height / 2, 'ybot').play('Idle');
        obj.eightDirection = this.plugins.get('rexEightDirection').add(obj, {
            dir: 3,
			speed: 500,
            rotateToDirection: false,
            cursorKeys: joyStick.createCursorKeys(),
        });
        obj.body
            .setSize(30, 30)
            .setCollideWorldBounds();
			
		joyStick.on('update', function(){
			let force = joyStick.force;
			obj.play(force === 0 ? 'Idle' : 'Walking', true);
		});
    }

    update() {}
}

