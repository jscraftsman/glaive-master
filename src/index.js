import Phaser from "phaser";

import MainScene from "../src/js/scenes/main.js";
	
import EightDirectionPlugin from '../src/js/plugins/rexeightdirectionplugin.min.js';
import VirtualJoyStickPlugin from '../src/js/plugins/rexvirtualjoystickplugin.min.js';
import AnchorPlugin from '../src/js/plugins/rexanchorplugin.min.js'; 

const config = {
    type: Phaser.AUTO,
    parent: 'glaive-master',
    width: 1920,
    height: 1080,
    scale: {
        mode: Phaser.Scale.HEIGHT_CONTROLS_WIDTH ,
        autoCenter: Phaser.Scale.CENTER_BOTH,
    },
    scene: MainScene,
    backgroundColor: 0x333333,
    physics: {
        default: 'arcade',
        arcade: {
            debug: true // TEMP
        }
    },
    plugins: {
        global: [{
                key: 'rexEightDirection',
                plugin: EightDirectionPlugin,
                start: true
            },
            {
                key: 'rexVirtualJoyStick',
                plugin: VirtualJoyStickPlugin,
                start: true
            },
			{
				key: 'rexAnchor',
				plugin: AnchorPlugin,
				start: true
			}
        ]
    }
};

const game = new Phaser.Game(config);
